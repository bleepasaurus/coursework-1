#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "product.h"

/*
  CW1.cpp
  author: M00692387
  Created: 05/01/2021
  Last modified: 21/01/2021
*/


template<typename T>
T check(T minimum, T maximum, std::string str) {
	while(true){
		T testValue;
		std::cout<< "Enter the "<< str <<": ";
		std::cin >> testValue;
		if(!std::cin){
			std::cin.clear(); // reset failbit
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			std::cout << "Wrong input try again" << std::endl;
		}
		else
			if(testValue > minimum && testValue <maximum)
				return testValue;
			else
				std::cout << "Wrong input try again" << std::endl;
	}
}

template<typename T>
std::string addSales(T* ptr, int &itemsSold){
	// Enters a lines into the sales file/vector with format of{ Name, Cost of item, amountsold, total profit }
	std::string cost = std::to_string(ptr->getCost()), sold = 
	std::to_string(itemsSold);
	
	std::string report = ptr->getName()+","+cost+","+sold+","+
	std::to_string((ptr->getCost())*itemsSold);
	std::cout << report << std::endl;
}

template<typename T>
void changeStock(T* ptr, int option){
	if( option == 1 ){
		int amount = check<int>(-1, (ptr)->getStock(), "amount of stock sold");
		(ptr)->setStock((ptr)->getStock()-amount);
	}
	else if( option == 2 ){
		int amount = check<int>(-1, 100000, "amount of new stock");
		(ptr)->setStock((ptr)->getStock()+amount);
	}
	else if( option == 3 ){
		int amount = check<int>(-1, 100000, "correct amount");
		(ptr)->setStock(amount);
	}
}

int checkId(int productType){
	int id;
	while (true){
		above:
		std::cin >> id;
		if(!(0 < id && id < 9999))
			continue;
		if(productType == 1)
			id += 200000;
		else if(productType == 2)
			id += 400000;
		else if(productType == 3)
			id += 500000;
		else if(productType == 4)
			id += 600000;
		
		return id;
	}
}

/*
	String confirmation function
	@param none
	@return returns an std::string
*/
std::string validation(){
	std::string entry, yesNo;
	while(true){
		std::cin.ignore();
		std::getline(std::cin, entry);
		std::cout << "\nYou entered: \"" << entry << "\" is this fine?(Y/N): ";
		std::cin >> yesNo;
		
		if(yesNo == "y" || yesNo == "yes" || yesNo == "Yes" || yesNo == "Y")
			return entry;
		std::cout << "Enter again: ";
	}
}

/*
	add new class and ask details of each product
	@param none
	@return none
*/
std::string addItems(){
	int option, id, stock;
	std::string strs[5];
	double cost;
	std::cout << "What type of product is being added?\n1. CD\n2. DVD\n3. Book\n4. Magazine\n(enter the number):";
	std::cin >> option;
	
	//input section of for class atributes
	std::cout << "\nEnter the following details:" << std::endl;
	std::cout <<"For a music disc enter the Album/Song name || For a book/Magazine enter it's title: ";
	strs[0] = validation();
	std::cout << "For a music disc enter the Artist/Composer name || For a book enter the author || For a magazine enter the publisher: ";
	strs[1] = validation();
	std::cout << "For a music disc enter the genre || For a book/Magazine enter the ISBN / ISSN respectively: ";
	strs[2] = validation();
	std::cout << "For a music disc enter the publisher or record label || For a book enter the publisher || For a magazine enter the the volume issue for the magazine: ";
	strs[3] = validation();
	std::cout << "For a book / magazine enter the genre(Enter blank for a music disc): ";
	strs[4] = validation();
	std::cout << "The current id of the new product(only 4-digits): ";
	id = checkId(option);
	cost = check<double>(-1.00, 100000.00, "cost of each item");
	stock = check(-1, 10000, "current stock of the item");
	if(option == 1){
		Cd sample(strs[0], strs[1], strs[2], strs[3], id, cost, stock);
		return sample.toString();
	}
	else if(option == 2){
		Dvd sample(strs[0], strs[1], strs[2], strs[3], id, cost, stock);
		return sample.toString();
	}
	else if(option == 3){
		Book sample(strs[0], strs[1], strs[2], strs[3], strs[4], id, cost, stock);
		return sample.toString();
	}
	else if(option == 4){
		Magazine sample(strs[0], strs[1], strs[2], strs[3], strs[4], id, cost, stock);
		return sample.toString();
	}
}


TEST_CASE("testing sell items use case", "[selection]")
{
	//Only the stock values are needed for the test
  Cd* s1 = new Cd("Test", "Test", "Test", "Test", 0002, 12.50, 25);
	Dvd* s2 = new Dvd("Test", "Test", "Test", "Test", 0002, 12.50, 35);
	Book* s3 = new Book("Test", "Test", "Test", "Test", "Test", 0002, 12.50, 45);
	Magazine* s4 = new Magazine("Test", "Test", "Test", "Test", "Test", 0002, 12.50, 55);
	
	// Using sell items part with an amount of *10*
	changeStock(s1, 1);
	changeStock(s2, 1);
	changeStock(s3, 1);
	changeStock(s4, 1);
	
  REQUIRE( (s1->getStock()==15) );
	REQUIRE( (s2->getStock()==25) );
	REQUIRE( (s3->getStock()==35) );
	REQUIRE( (s4->getStock()==45) );
	
}

TEST_CASE("testing restock items use case", "[selection]")
{
	//Only the stock values are needed for the test
  Cd* s1 = new Cd("Test", "Test", "Test", "Test", 0002, 12.50, 25);
	Dvd* s2 = new Dvd("Test", "Test", "Test", "Test", 0002, 12.50, 35);
	Book* s3 = new Book("Test", "Test", "Test", "Test", "Test", 0002, 12.50, 45);
	Magazine* s4 = new Magazine("Test", "Test", "Test", "Test", "Test", 0002, 12.50, 55);
	
	// Using restock part with an amount of *10*
	changeStock(s1, 2);
	changeStock(s2, 2);
	changeStock(s3, 2);
	changeStock(s4, 2);
	
  REQUIRE( (s1->getStock()==35) );
	REQUIRE( (s2->getStock()==45) );
	REQUIRE( (s3->getStock()==55) );
	REQUIRE( (s4->getStock()==65) );
	
}

TEST_CASE("testing update stock use case", "[selection]")
{
	//Only the stock values are needed for the test
  Cd* s1 = new Cd("Test", "Test", "Test", "Test", 0002, 12.50, 25);
	Dvd* s2 = new Dvd("Test", "Test", "Test", "Test", 0002, 12.50, 35);
	Book* s3 = new Book("Test", "Test", "Test", "Test", "Test", 0002, 12.50, 45);
	Magazine* s4 = new Magazine("Test", "Test", "Test", "Test", "Test", 0002, 12.50, 55);
	
	// Using sell update stock with an amount of *10*
	changeStock(s1, 3);
	changeStock(s2, 3);
	changeStock(s3, 3);
	changeStock(s4, 3);
	
  REQUIRE( (s1->getStock()==10) );
	REQUIRE( (s2->getStock()==10) );
	REQUIRE( (s3->getStock()==10) );
	REQUIRE( (s4->getStock()==10) );
	
}

TEST_CASE("creating object function", "[selection]")
{
	//Only the stock values are needed for the test
  Cd* s1 = new Cd("Test", "Test", "Test", "Test", 200002, 12.50, 25);
	Dvd* s2 = new Dvd("Test", "Test", "Test", "Test", 400002, 12.50, 35);
	Book* s3 = new Book("Test", "Test", "Test", "Test", "Test", 500002, 12.50, 45);
	Magazine* s4 = new Magazine("Test", "Test", "Test", "Test", "Test", 600002, 12.50, 55);
	
  REQUIRE( s1->toString() == addItems() );
	
}