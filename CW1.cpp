#include "product.h"
#include <sstream>
#include <string.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <iterator>
#include <limits>


/*
  CW1.cpp
  author: M00692387
  Created: 05/01/2021
  Last modified: 22/01/2021
*/


// fileContent- stock.csv,	saleContent- sales.csv
std::vector<std::string> saleContent;
std::vector<std::string> fileContent;
//pointers to store values between functions/units
Cd* cdPtr = NULL;
Dvd* dvdPtr = NULL;
Book* bookPtr = NULL;
Magazine* magazinePtr = NULL;

/*
	Filestream reading function
	@param input file stream and global string vector
	@return none but fills out the stop-gap vectors
*/
void initialRead(std::ifstream &inFile, std::vector<std::string> &sample, std::string &filename) {
	std::string contents;
	inFile.open(filename, std::ios::in);
	if(inFile.peek() == std::ifstream::traits_type::eof()){
		// Used to fill an empty created file with the respective column headers 
		if(filename == "stocks.csv"){
			sample.push_back("CD,Name,Artist,Genre,Publisher,Id,Cost per unit,Stock");
			sample.push_back("");
			sample.push_back("DVD,Name,Artist,Genre,Publisher,Id,Cost per unit,Stock");
			sample.push_back("");
			sample.push_back("Book,Title,Author,ISBN,Publisher,Genre,Id,Cost per unit,Stock");
			sample.push_back("");
			sample.push_back("Magazine,Title,Publisher,ISSN,Volume and Issue,Genre,Id,Cost per unit,Stock");
			sample.push_back("");
		}
		else {
			sample.push_back("Name, Cost, Amount sold, Revenue");
		}
	}
	// Simple line by line reading
	while (std::getline(inFile, contents))
    sample.push_back(contents);
	inFile.close();
}

/*
	Stock vector(fileContent) updating function
	@param the id of a class and the output of the toString function
	@return none but edits and replaces the vector content
*/
void updateLog(int id, std::string record){
	auto itPos = fileContent.begin();
	int i;
	// For loop for when updating an existing record after a stockChange function
	for(i=0; i<fileContent.size(); i++){
		if (fileContent[i].find(std::to_string(id)) != std::string::npos) {
			fileContent[i] = record;
			return;
		}
	}
	// For loop for adding a new record after addItems function
	for(i=0; i<fileContent.size(); i++){
		if(std::to_string(id).substr(0, 2)=="20" && fileContent[i].find("CD") != std::string::npos)
			itPos = fileContent.begin() + (i+1);
		else if(std::to_string(id).substr(0, 2)=="40" && fileContent[i].find("DVD") != std::string::npos)
			itPos = fileContent.begin() + (i+1);
		else if(std::to_string(id).substr(0, 2)=="50" && fileContent[i].find("Book") != std::string::npos)
			itPos = fileContent.begin() + (i+1);
		else if(std::to_string(id).substr(0, 2)=="60" && fileContent[i].find("Magazine") != std::string::npos)
			itPos = fileContent.begin() + (i+1);
	}
	fileContent.insert(itPos, record);
}

/*
	Record/Object retreiving function
	@param id for the object
	@return none but adds an address to the respective pointer
*/
bool getProductById(int id){
	std::string line;
	std::vector<std::string> vec;
	std::string ID = std::to_string(id);
	
	// Breaking each line record into words separated by a comma(CSV) and checking for the id
	for(int i=0; i<fileContent.size(); i++){
		line = fileContent[i];
		std::istringstream iss(line);
		while (iss.good()) {
			std::string substr;
			getline(iss, substr, ',');
			vec.push_back(substr);
		}
		int size = vec.size();
		// size is used to get the index within the record backwards
		// [size-7], [size-6], [size-5], [size-4], [size-3], [size-2], [size-1]
		// albumName artistName genre    publisher    id       cost      stock
		if(line.find(ID) != std::string::npos && ID.substr(0, 2)=="20"){
			cdPtr = new Cd(vec[size-7], vec[size-6], vec[size-5], vec[size-4],
			std::stoi(vec[size-3]), std::stod(vec[size-2]), std::stoi(vec[size-1]));
			return true;
	  }
		// "20" "40" "60" "80" Cd Dvd Book Magazine
		else if(line.find(ID) != std::string::npos && ID.substr(0, 2)=="40") {
			dvdPtr = new Dvd(vec[size-7], vec[size-6], vec[size-5], vec[size-4],
			std::stoi(vec[size-3]), std::stod(vec[size-2]), std::stoi(vec[size-1]));
		  return true;
		}
		else if(line.find(ID) != std::string::npos && ID.substr(0, 2)=="50") {
			bookPtr = new Book(vec[size-8], vec[size-7], vec[size-6], vec[size-5], vec[size-4],
			std::stoi(vec[size-3]), std::stod(vec[size-2]), std::stoi(vec[size-1]));
			return true;
		}
		else if(line.find(ID) != std::string::npos && ID.substr(0, 2)=="60") {
			magazinePtr = new Magazine(vec[size-8], vec[size-7], vec[size-6], vec[size-5],
			vec[size-4], std::stoi(vec[size-3]), std::stod(vec[size-2]), std::stoi(vec[size-1]));
			return true;
		}
		vec.clear();
	}
	std::cout << "\nNo Object by that Id present" << std::endl;
	return false;
}

/*
	Check function
	@param maximum and minimum values along with a string for custom requests
	@return an integer within the range
*/
template<typename T>
T check(T minimum, T maximum, std::string str) {
	while(true){
		T testValue;
		std::cout<< "Enter the "<< str <<": ";
		std::cin >> testValue;
		if(!std::cin){ // checking cin failure if string is entered instead of int/double
			std::cin.clear(); // reset failbit
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			std::cout << "Wrong input try again" << std::endl;
		}
		else
			if(testValue > minimum && testValue <maximum)
				return testValue;
			else
				std::cout << "Wrong input try again" << std::endl;
	}
}

/*
	Filestream reading function
	@param object pointer and sold items from sellItems function
	@return none but adds records the sales vector
*/
template<typename T>
void addSales(T* ptr, int &itemsSold){
	// Enters a lines into the sales file/vector with format of{ Name, Cost of item, amountsold, total profit }
	std::string cost = std::to_string(ptr->getCost()), sold = 
	std::to_string(itemsSold);
	
	std::string report = ptr->getName()+","+cost+","+sold+","+
	std::to_string((ptr->getCost())*itemsSold);
	saleContent.push_back(report);
}

/*
	Combined stock changing function for reducing restocking and updating as 1 2 3 respectively
	@param object pointer and option to choose operation
	@return none but it changes the record vectors
*/
template<typename T>
void changeStock(T* ptr, int option){
	if( option == 1 ){
		int amount = check<int>(-1, (ptr)->getStock(), "amount of stock sold");
		(ptr)->setStock((ptr)->getStock()-amount);
		addSales(ptr, amount);
	}
	else if( option == 2 ){
		int amount = check<int>(-1, 100000, "amount of new stock");
		(ptr)->setStock((ptr)->getStock()+amount);
	}
	else if( option == 3 ){
		int amount = check<int>(-1, 100000, "correct amount");
		(ptr)->setStock(amount);
	}
	updateLog(ptr->getId(), (ptr)->toString());
}

/*
	ID assigning function for addItems function
	@param product type from addItems
	@returns the finalised id 
*/
int checkId(int productType){
	int id;
	while (true){
		above:
		std::cin >> id;
		if(!(0 < id && id < 9999))
			continue;
		if(productType == 1)
			id += 200000;
		else if(productType == 2)
			id += 400000;
		else if(productType == 3)
			id += 500000;
		else if(productType == 4)
			id += 600000;
		for (int i=0; i<fileContent.size(); i++)
			if(fileContent[i].find(std::to_string(id)) != std::string::npos)
				goto above;
		return id;
	}
}

/*
	String confirmation function
	@param none
	@return returns an std::string
*/
std::string validation(){
	std::string entry, yesNo;
	while(true){
		std::cin.ignore();
		std::getline(std::cin, entry);
		std::cout << "\nYou entered: \"" << entry << "\" is this fine?(Y/N): ";
		std::cin >> yesNo;
		
		if(yesNo == "y" || yesNo == "yes" || yesNo == "Yes" || yesNo == "Y")
			return entry;
		std::cout << "Enter again: ";
	}
}

/*
	add new class and ask details of each product
	@param none
	@return none
*/
void addItems(){
	int option, id, stock;
	std::string strs[5];
	double cost;
	std::cout << "What type of product is being added?\n1. CD\n2. DVD\n3. Book\n4. Magazine\n(enter the number):";
	std::cin >> option;
	
	//input section of for class atributes
	// use of validation utility function to confirm user options
	std::cout << "\nEnter the following details:" << std::endl;
	std::cout <<"For a music disc enter the Album/Song name || For a book/Magazine enter it's title: ";
	strs[0] = validation();
	std::cout << "For a music disc enter the Artist/Composer name || For a book enter the author || For a magazine enter the publisher: ";
	strs[1] = validation();
	std::cout << "For a music disc enter the genre || For a book/Magazine enter the ISBN / ISSN respectively: ";
	strs[2] = validation();
	std::cout << "For a music disc enter the publisher or record label || For a book enter the publisher || For a magazine enter the the volume issue for the magazine: ";
	strs[3] = validation();
	std::cout << "For a book / magazine enter the genre(Enter blank for a music disc): ";
	strs[4] = validation();
	std::cout << "The current id of the new product(only 4-digits): ";
	id = checkId(option);
	cost = check<double>(-1.00, 100000.00, "cost of each item");
	stock = check(-1, 10000, "current stock of the item");
	
	// Form object from user data and push to stock file vector (fileContent)
	if(option == 1){
		Cd sample(strs[0], strs[1], strs[2], strs[3], id, cost, stock);
		updateLog(id, sample.toString());
	}
	else if(option == 2){
		Dvd sample(strs[0], strs[1], strs[2], strs[3], id, cost, stock);
		updateLog(id, sample.toString());
	}
	else if(option == 3){
		Book sample(strs[0], strs[1], strs[2], strs[3], strs[4], id, cost, stock);
		updateLog(id, sample.toString());
	}
	else if(option == 4){
		Magazine sample(strs[0], strs[1], strs[2], strs[3], strs[4], id, cost, stock);
		updateLog(id, sample.toString());
	}
}

/*
	Vector display function. It displays the content in the vector not the file
	@param file vector to display
	@return none
*/
void showData(std::vector<std::string> &sample){
	std::cout << "\n" << std::endl;
	for(int i=0; i<sample.size(); i++) {
		std::cout << sample[i] << std::endl;
	}
	std::cout << "\n" << std::endl;
}

/*
	File writing function
	@param an output file stream the vector to write from and the filename of the receiving file
	@return none
*/
void endingWrite(std::ofstream &ofs, std::string &filename, std::vector<std::string> &sample){
	ofs.open(filename, std::ofstream::out | std::ofstream::trunc);
	std::ostream_iterator<std::string> output_iterator(ofs, "\n");
  std::copy(sample.begin(), sample.end(), output_iterator);
	ofs.close();
}

/*
	Stop gap function to call file and turn pointer to NULL
	@param function to call, the object pointer and the integer option for changeStock
	@return none
*/
template<typename T>
void callFunction(void (*func)(T*, int), T* ptr, int option){
	func(ptr, option);
	delete ptr;
	ptr = nullptr;
}

int main() {
	// Initial reading and setup along with first display of existing records
	std::string stocks = "stocks.csv";
	std::string sales = "sales.csv";
	
	// Initialisations of file streams
	std::ifstream reader;
	std::ofstream writer;
	
	// First read and file access to write file content into the vector for easy access
	initialRead(reader, fileContent, stocks);
	reader.clear();
	initialRead(reader, saleContent, sales);
	
	// show data used as user reference
	showData(fileContent);
	bool end = false, validId = false;
	int id, stock;

	while(!end){
		int menuOption;
		std::cout << "\n\n--------------------------------------------------" << std::endl;
		std::cout << "\nStock management system 1.0" << std::endl;
		std::cout << "Which action to perform?" << std::endl;
		std::cout << "\t1 to Add new products," << std::endl;
		std::cout << "\t2 to View a sales report," << std::endl;
		std::cout << "\t3 to Update stock quantity," << std::endl;
		std::cout << "\t4 to reduce stock of products(sold items)," << std::endl;
		std::cout << "\t5 to restock curent products)" << std::endl;
		std::cout << "\t6 to view current stock information " << std::endl;
		std::cout << "\t7 to end the program " << std::endl;
		std::cout << "? : ";
		
		//Used to set range and invalid inputs for the menu option
		menuOption = check(0, 8, "operation number you want"); // for range of operations

		switch(menuOption){
		case 1:
			addItems();
			break;
		case 2:
			showData(saleContent);
			break;
		case 3:
			id = check(199999, 700000, "id of the product"); // using id range
			validId = getProductById(id);
			
			if(std::to_string(id).substr(0, 2)=="20" && validId)
				callFunction<Cd>(changeStock, cdPtr, 3); // calling updateStock portion with 3
			else if(std::to_string(id).substr(0, 2)=="40" && validId)
				callFunction<Dvd>(changeStock, dvdPtr, 3);
			else if(std::to_string(id).substr(0, 2)=="50" && validId)
				callFunction<Book>(changeStock, bookPtr, 3);
			else if(std::to_string(id).substr(0, 2)=="60" && validId)
				callFunction<Magazine>(changeStock, magazinePtr, 3);
		break;
		
		case 4:
			id = check(199999, 700000, "id of the product"); // using id range
			validId = getProductById(id);
			
			if(std::to_string(id).substr(0, 2)=="20" && validId)
				callFunction<Cd>(changeStock, cdPtr, 1); // calling sell items portion with 1
			else if(std::to_string(id).substr(0, 2)=="40" && validId)
				callFunction<Dvd>(changeStock, dvdPtr, 1);
			else if(std::to_string(id).substr(0, 2)=="50" && validId)
				callFunction<Book>(changeStock, bookPtr, 1);
			else if(std::to_string(id).substr(0, 2)=="60" && validId)
				callFunction<Magazine>(changeStock, magazinePtr, 1);
			break;
			
		case 5:
			id = check(199999, 700000, "id of the product"); // using id range
			validId = getProductById(id);
			
			if(std::to_string(id).substr(0, 2)=="20" && validId)
				callFunction<Cd>(changeStock, cdPtr, 2);				// calling restock items portion with 2
			else if(std::to_string(id).substr(0, 2)=="40" && validId)
				callFunction<Dvd>(changeStock, dvdPtr, 2);
			else if(std::to_string(id).substr(0, 2)=="50" && validId)
				callFunction<Book>(changeStock, bookPtr, 2);
			else if(std::to_string(id).substr(0, 2)=="60" && validId)
				callFunction<Magazine>(changeStock, magazinePtr, 2);
			break;
			
		case 6:
			showData(fileContent); // simple display function with stock vector
			break;
			
		case 7:
			end = true;
			break;
		default:
			std::cout << "\nInvalid input(enter a number)" << std::endl;
		}
	}
	std::cout << "\nProgram closing" << std::endl;
	// Ending write functions to update both files with the vector values
	endingWrite(writer, stocks, fileContent);
	endingWrite(writer, sales, saleContent);
}