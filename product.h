#ifndef PRODUCT_H
#define PRODUCT_H
#include <string>
/*
  product.h
  Author: M00692387
  Created: 05/01/2021
  Last updated: 16/01/2021
*/



// Main layout for all classes: variables, constructors, accessors, mutators
class Product 
{
private:
  int id;
  double cost;
  int stock;

public:  
  Product(int id, double cost, int stock);
  Product(int id);
  Product();

  int getId();
  double getCost();
  int getStock();
    
  void setStock (int stock);
  void setCost (double cost);
  void setId (int id);
};

/*
	
*/

class Cd : public Product 
{
private:
  std::string albumName;
  std::string artistName;
  std::string genre;
  std::string publisher;

public:
  Cd(std::string albumName, std::string artistName, std::string genre,
 std::string publisher, int id, double cost, int stock );
  Cd();

  std::string getName();
  std::string getArtistName();
  std::string getGenre();
  std::string getPublisher();
	std::string toString();

  void setAlbumName (std::string albumName);
  void setArtistName (std::string artistName);
  void setGenre (std::string genre);
  void setPublisher (std::string publisher);
};


class Dvd : public Product 
{
private:
  std::string albumName;
  std::string artistName;
  std::string genre;
  std::string publisher;

public:
  Dvd(std::string albumName, std::string artistName, std::string genre,
 std::string publisher, int id, double cost, int stock);
  Dvd();

  std::string getName();
  std::string getArtistName();
  std::string getGenre();
  std::string getPublisher();
	std::string toString();

  void setAlbumName (std::string albumName);
  void setArtistName (std::string artistName);
  void setGenre (std::string genre);
  void setPublisher (std::string publisher);
};


class Book : public Product 
{
private:
  std::string title;
  std::string author;
  std::string ISBN;
  std::string publisher;
  std::string genre;

public:
  Book(std::string title, std::string author, std::string ISBN, 
std::string publisher, std::string genre, int id, double cost, int stock);
  Book();

  std::string getName();
  std::string getAuthor();
  std::string getISBN();
  std::string getPublisher();
  std::string getGenre();
	std::string toString();

  void setTitle (std::string title);
  void setAuthor (std::string author);
  void setISBN (std::string ISBN);
  void setPublisher (std::string publisher);
  void setGenre (std::string genre);
};


class Magazine : public Product 
{
private:
  std::string title;
  std::string publisher;
  std::string ISSN;
  std::string volumeIssue;
  std::string genre;

public:
  Magazine( std::string title, std::string publisher, std::string ISSN,
 std::string volumeIssue, std::string genre, int id, double cost, int stock );
  Magazine();
  
  std::string getName();
  std::string getPublisher();
  std::string getISSN();
  std::string getVolumeIssue();
  std::string getGenre();
	std::string toString();

  void setTitle (std::string title);
  void setPublisher (std::string publisher);
  void setISSN (std::string ISSN);
  void setVolumeIssue (std::string volumeIssue);
  void setGenre (std::string genre);
};
#endif
